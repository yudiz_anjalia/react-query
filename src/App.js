import React from 'react';
import './App.css';
import { useQuery } from 'react-query';
import axios from 'axios';

function App() {

  const { isLoading, error, data } = useQuery("users", () => 
  axios
    .get("https://jsonplaceholder.typicode.com/users")
    .then((res) => res.data)
  ) 

  if(isLoading) return 'Loading...'
  if(error) return "An error has occurred:" + error.message

  return (
    <div className="App">
      <br />
      <h1>React Query</h1>
      <br ß/>
      {data.map((user) => (
        <h3 key={user.id}>{user.name}</h3>
      ))}
    </div>
  );
}

export default App;
